
public class Balls {

   enum Color {green, red};
   
   public static void main (String[] param) {
      Color[] colors = new Color[] {Color.green, Color.red, Color.green, Color.red, Color.green, Color.green, Color.red, Color.green};

      reorder(colors);
   }

   public static void reorder (Color[] balls) {
      int redBalls = 0;
      int greenBalls = 0;
      for(Color ball : balls) {

         if(ball == Color.green) {
            greenBalls++;
         } else {
            redBalls++;
         }
      }
      for(int j = 0; j < redBalls; j++) {
         balls[j] = Color.red;
      }
      for(int i = redBalls; i < greenBalls+redBalls; i++) {
         balls[i] = Color.green;
      }
   }
}

